Disqus_thread
=============

Download a Disqus comment thread and display it as a formatted text file.
You will need a Disqus API key.

Get your Disqus API key
-----------------------
Go to https://disqus.com/api/applications/ and log in with your Disqus credentials.  
Register a new application, and in the *Settings* tab add disqus.com to the domains.  
The *Applications* page should now contain both your private and public API keys.  
Save your API public key in a file named **api_key.txt** in the same folder as disqus_thread.py.

Archive a Disqus comment thread in json format
----------------------------------------------
Running

    python3 disqus_thread.py http://www.my.favourite.site.com/some/path/

will produce a text file named path.json

You can change the output file by issuing the command

    python3 disqus_thread.py http://www.my.favourite.site.com/some/path/ filename

From json to formatted text
---------------------------
The command

    python3 threadify.py filename.json

produces a text file named filename.txt

You can change the output file by issuing the command

    python3 threadify.py http://www.my.favourite.site.com/some/path/ filename
