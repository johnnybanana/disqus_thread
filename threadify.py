#! /usr/bin/python3

import sys
import json
import html
from dateutil.parser import parse
from textwrap import TextWrapper

def print_node(thread, post, level,
               file=sys.stdout, maxlevel=10, tabwidth=2, width=79):
    TAB = ' ' * (tabwidth - 1) + '|'
    indent = min(level, maxlevel)
    author = post['author']
    message = post['message']
    wrapper = TextWrapper(initial_indent=indent*TAB,
                          subsequent_indent=indent*TAB,
                          replace_whitespace=False,
                          width=width,
                          break_long_words=False,
                          break_on_hyphens=False)
    print(indent * TAB, file=file)
    print(wrapper.fill(author), file=file)
    print(wrapper.fill('-' * len(author)), file=file)
    for par in message.splitlines():
        if par:
            print(wrapper.fill(par), file=file)
        else:
            print(indent * TAB, file=file)
    print(indent * TAB, file=file)
    print(wrapper.fill('~' * (width - len(indent*TAB))), file=file)
    for reply in post['children']:
        print_node(thread, reply, level+1, file, maxlevel, tabwidth, width)
    # if post == thread[post['parent_id']]['children'][-1]:
    #     print((indent-1) * TAB + '~' * (width - len((indent-1)*TAB)))


def print_thread(data, filename=sys.stdout):
    # data: json array of objects, each representing a post
    #       (array must be named "response")
    # prints formatted thread to `filename`
    posts = []
    for msg in data['response']:
        id_ = int(msg['id'])
        parent_id = 0 if msg['parent'] is None else int(msg['parent'])
        children = []
        date = parse(msg['createdAt'])
        author = msg['author']['name']
        message = html.unescape(msg['raw_message'])
        post = dict(
            parent_id=parent_id,
            children=children,
            date=date,
            author=author,
            message=message
        )
        posts.append((id_, post))
    posts.sort(key=lambda p: p[1]['date'])

    thread = {0: dict(children=[])}
    for id_, post in posts:
        thread[id_] = post
        parent_id = post['parent_id']
        thread[parent_id]['children'].append(post)
        thread[parent_id]['children'].sort(key=lambda p: p['date'])

    for msg in thread[0]['children']:
        print_node(thread, msg, 0, filename)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: " + sys.argv[0] + " somefile.json [, output_file]")
    else:
        inputfile = sys.argv[1]
        if len(sys.argv) < 3:
            outputfile = inputfile.strip(".json") + ".txt"
        else:
            outputfile = sys.argv[2]
        with open(inputfile) as f:
            data = json.load(f)

        with open(outputfile, 'w') as f:
            print_thread(data, f)
