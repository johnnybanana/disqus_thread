#! /usr/bin/python3

import sys
import requests
import json
from urllib.parse import urlparse

def archive(link, filename=None, key=None):
    if filename is None:
        filename = urlparse(link).path.strip('/').split('/')[-1] + ".json"
    if key is None:
        with open('api_key.txt') as keyfile:
            key = keyfile.readline().strip()
    api_url = "https://disqus.com/api/3.0/posts/list.json"
    params = {
        'api_key': key,
        'thread': 'link:' + link,
        'include': ["approved", "deleted"],
        'limit': '100',
        'order': 'asc',
        'cursor': ''
    }

    posts = dict({"response": []})
    hasNext = True
    while hasNext:
        res = requests.get(api_url, params=params)
        if not res.ok:
            # check if you exceeded hourly request limit
            tmp = res.json()
            if 'code' in tmp and tmp['code'] == 13:
                print(tmp['response'], file=sys.stderr)
                sys.exit(13)
            # maybe `link` is redirected: in that case, this gets final url
            probe = requests.get(link)
            params['thread'] = 'link:' + probe.url
            res = requests.get(api_url, params=params)
        data = res.json()
        posts['response'].extend(data['response'])
        hasNext = data['cursor']['hasNext']
        params['cursor'] = data['cursor']['next']
    with open(filename, 'w') as f:
        json.dump(posts, f)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: " + sys.argv[0] + " http://your.link.here/ [, filename]")
    else:
        archive(*sys.argv[1:])
